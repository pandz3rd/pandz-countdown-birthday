const daysEl = document.getElementById('days')
const hoursEl = document.getElementById('hours')
const minutesEl = document.getElementById('minutes')
const secondsEl = document.getElementById('seconds')

const myBirthday = '12 Feb 2021'

const countdownTimer = () => {
  const currentDate = new Date()
  const myBirthdayDate = new Date(myBirthday)

  const totalSeconds = (myBirthdayDate - currentDate) / 1000
  const days = Math.floor(totalSeconds / 3600 / 24)
  const hours = formatTime(Math.floor(totalSeconds / 3600) % 24)
  const minutes = formatTime(Math.floor(totalSeconds / 60) % 60)
  const seconds = formatTime(Math.floor(totalSeconds) % 60)

  daysEl.innerHTML = days
  hoursEl.innerHTML = hours
  minutesEl.innerHTML = minutes
  secondsEl.innerHTML = seconds

  console.log(days, hours, minutes, seconds)
}

const formatTime = (time) => {
  return time < 10 ? `0${time}` : time
}

countdownTimer()

setInterval(countdownTimer, 1000)
